import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch,Button ,Title,} from 'native-base';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import {StyleSheet,Image} from 'react-native';
// import Flag from 'react-native-flags';
export default class Setting extends Component {
  render() {
    return (
      <Container>
         <Header style={{backgroundColor:'#EE9A21'}}>
          <Left>
            <Button transparent onPress={()=>this.props.navigation.goBack()}>
            <Image
                style={{ marginLeft:10,width: 20, height: 20, }}
                source={require('../Image/icons8-left-100-white.png')}
                        />
            </Button>
          </Left>
          <Body>
            <Title style={{color:'white',fontSize:25}}>Setting</Title>
          </Body>
          <Right>
            <Button transparent onPress={()=>this.props.navigation.navigate('Notification')}>
            {/* <Icon type="FontAwesome" name="bell" /> */}
            </Button>
          </Right>
        </Header>
        <Content>

        <ListItem icon>       
           <Body>
             <Text style={{color:'black',fontSize:18}}>Langauge</Text>
             <Text note style={{fontSize:11}}>
                   By default is Khmer
             </Text>
           </Body>
           
           <RadioGroup
              style={styles.ratio}>     
                <RadioButton value={'Khmer'} >
                <Image source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Flag_of_Cambodia.svg/510px-Flag_of_Cambodia.svg.png'}} style={{width: 30, height: 20}} />
                </RadioButton>
                      
                <RadioButton value={'English'}  >
                <Image source={{uri: 'https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/510px-Flag_of_the_United_Kingdom.svg.png'}} style={{width: 30, height: 20}} />
                </RadioButton>                 
            </RadioGroup>

          
         </ListItem>

          <ListItem icon >          
            <Body>
              <Text style={{color:'black',fontSize:18}}>Notication</Text>
              <Text note style={{fontSize:11}}>
                    Allows KNONG DAI to send notications
              </Text>
            </Body>
            <Right>
              <Switch value={true} />
            </Right>
          </ListItem>

          <ListItem icon onPress={()=>this.props.navigation.navigate('Policy')}>  
            <Body>
              <Text>Policy</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>


          <ListItem icon onPress={()=>this.props.navigation.navigate('About')}>
            
            <Body>
              <Text>About Us</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}
const styles=StyleSheet.create({
  ratio:{
    flex:1,
    flexDirection: 'row',
    marginRight:20,

  }

})