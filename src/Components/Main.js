import React, { Component } from 'react'
import { Text, View,Image} from 'react-native'
import Home from './Home';
import One from "./One";
import Two from "./Two";
import {
  Container,
  Header,
  Item,
  Button,
  Card,
  CardItem,
  Left,
  Right,
  Body,
  Content,
  Icon
} from 'native-base';
import { createDrawerNavigator, createAppContainer,DrawerItems } from 'react-navigation'
export default class Main extends Component {
  render() {
    return (
      <MyApp />
    )
  }
}

const CustomeDrawerContent = (props) => (
  <Container>
    <Header style={{height:200}}>
      <Body>
        <Image source={require('../Image/kd-circle.png')}
          style={{ width:150, height:150 }}
        />
      </Body>
    </Header>
    <Content>
      <DrawerItems  {...props}/>
    </Content>
  </Container>
)
const MyDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: Home,
  },
  One: {
    screen: One
  }
},
{
  initialRouteName:'Home',
  contentComponent:CustomeDrawerContent
}
)
const MyApp = createAppContainer(MyDrawerNavigator);



