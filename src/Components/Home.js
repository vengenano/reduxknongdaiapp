import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, Platform } from 'react-native';
import {
    Container,
    Header,
    Item,
    Button,
    Card,
    CardItem,
    Left,
    Right,
    Body,
    Content,
    Icon
} from 'native-base';
export default class Home extends Component {

    static navigationOptions={
        drawerIcon:(
            <Image source={require('../Image/icons8-home-filled-100.png')}
          style={{ width:35, height:35 }}
        />
        )
    }
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        // const {container}=styles
        return (
            <Container>
                <Header>
                    <Left>
                        <Icon name="ios-menu" onPress={() =>
                            this.props.navigation.openDrawer()} />
                    </Left>
                </Header>
                <Content contentContainerStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>Home Screen</Text>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    // container:{
    //     flex:1,
    //     justifyContent: 'center',
    //     alignItems: 'center',
    // }
})
